var nilai_awal = 1
nilai_cetak = 2
nilai_batas = 11
console.log("Looping Pertama")
while (nilai_awal < nilai_batas) {
    console.log(nilai_cetak + "-" + "I Love Coding")
    nilai_cetak = nilai_cetak + 2;
    nilai_awal++;
}

var nilai_awal = 20
nilai_batas = 10
nilai_cetak = 20
console.log("Looping Kedua")
while (nilai_awal > nilai_batas) {
    console.log(nilai_cetak + "-" + "I Love Coding")
    nilai_cetak = nilai_cetak - 2;
    nilai_awal--;
}